//
//    ViewController.swift
//    macOS11WindowTest
//
//    MIT License
//
//    Copyright (c) 2020 William Glynn Smith
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in all
//    copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//    SOFTWARE.
//

import Cocoa

class ViewController: NSViewController, NSTableViewDelegate, NSTableViewDataSource {
    
    private let users = ["Johnny Appleseed", "Jane Doe"]

    @IBOutlet weak var tableView: NSTableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let defaultSelectedRow = IndexSet(integer: 1)
        tableView.selectRowIndexes(defaultSelectedRow, byExtendingSelection: false)
    }
    
    func numberOfRows(in tableView: NSTableView) -> Int {
        return users.count + 1
    }
    
    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {

        if(row == 0) {
            return headerCell(titled: "Users", for: tableView)
        } else {
            return standardCell(titled: users[row - 1], imageName: "person.fill", for: tableView)
        }
    }
    
    func tableView(_ tableView: NSTableView, heightOfRow row: Int) -> CGFloat {
        if(row == 0) {
            return 18
        } else {
            return 28
        }
    }
    
    func tableView(_ tableView: NSTableView, shouldSelectRow row: Int) -> Bool {
        if(row == 0) {
            return false
        } else {
            return true
        }
    }
    
    func tableViewSelectionDidChange(_ notification: Notification) {
        print("User selected row for " + users[tableView.selectedRow - 1])
    }
    
    private func headerCell(titled title: String, for tableView: NSTableView) -> NSView? {
        let cell = tableView.makeView(withIdentifier: .init("Header"), owner: self) as? NSTableCellView
        cell?.textField?.stringValue = title
        return cell
    }
    
    private func standardCell(titled title: String, imageName: String, for tableView: NSTableView) -> NSView? {
        let cell = tableView.makeView(withIdentifier: .init("Cell"), owner: self) as? NSTableCellView
        cell?.imageView?.image = NSImage(systemSymbolName: imageName, accessibilityDescription: nil)
        cell?.textField?.stringValue = title
        return cell
    }
    
    
}
