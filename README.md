# macOS 11 Window Test

A simple project from my website that makes use of the new window styling options in macOS 11 Big Sur. Pay particular attention to the sidebar intent of the NSSplitView in the main storyboard. This is where most of the magic comes from.

If you figure out a way to achieve this with code alone (especially with SwiftUI), I'd love to hear it! Apple is still hammering out the documentation for the new APIs, so I'm pretty much in the dark.